# MultiplicaColorChallenge

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.6.

## Descripción de Aplicación

La aplicación denominada colorChallenge fue desarrollada en angular, para esta aplicación se intenta representar las vistas presentadas como recursos gráficos por Multiplica y se crea la lógica de una aplicación que consume datos sobre colores (id,nombre,años y Pantone), Una vez obtenidos estos colores se desarrolla un grid en el cual se presentan 9 recuadros los cuales como backgroud contiene el color decodificado de los datos obtenidos y presenta la distinta información de este como texto al dar click a uno de estos recuadros. Una vez realizada las funcionalidades del primer componente el segundo es similar solo se lanza el segundo componente, el cual tiene por bakcgroud el color decodificado obtenido anteriormente y el código hexadecimal que representa al color se copia en el portapapeles del usuario. Se desarrolla la aplicación enfocándose en representar de la mejor manera los recursos gráficos facilitados por multiplica para este desafío.

## Tecnologías/FrameWork Utilizadas

* Lenguaje TypeScript
* Framework angular para desarrollar la aplicación 
* Ngx-Pagination para la paginación
* Angular Material como Framework de CSS y para desarrollar la función del clipboard 
* Css y flexbox para los estilos
* HttpClient para consumir la API
* Git para el control de versiones (https://gitlab.com/fsotomayor30/color-challenge-web)


## Instalación de Dependencias y Correr el Proyecto

Para instalar las dependencias se debe estar en el fichero raíz del proyecto y ejecutar el comando `npm install` una vez termina la ejecución de este comando se ejecuta `ng serve` para correr el proyecto, este se podrá ver en `http://localhost:4200/`.

## URL Netlify

https://colors-multiplica.netlify.app/
